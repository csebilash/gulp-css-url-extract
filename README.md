# gulp-css-url-extract

This gulp task extracts all url found within a stylesheet (those within a url( ... ) declaration) into output.txt file. This includes images and fonts.

## Features

* Supports buffer.
* Filter by url extension.

## Install

Install this plugin with the command:

```js
npm install gulp-css-url-extract --save
```

## Usage

```js
var cssUrlExtract = require('gulp-css-url-extract');

//Without options
gulp.task('default', function () {
    return gulp.src('src/css/input.css')
        .pipe(cssUrlExtract())
});

//With options
gulp.task('default', function () {
    return gulp.src('src/css/input.css')
        .pipe(cssUrlExtract({
            extensionsAllowed: ['.png', '.jpg']
        }))
});
```


## License
Copyright (c) 2016 [Md Nazmul Hossain](https://github.com/gulp-css-url-extract) under the MIT License.
