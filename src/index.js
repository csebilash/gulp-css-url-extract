'use strict';
var fs = require('fs')
var path = require('path');
var mime = require('mime');
var util = require('util');
var Stream = require('stream').Stream;
var gutil = require('gulp-util');
var through = require('through2');
var async = require('async');


var reg = /url(?:\(['|"]?)(.*?)(?:['|"]?\))/ig;

var arr = [];

function gulpCssUrlExtract(opts) {
  opts = JSON.parse(JSON.stringify(opts || {}));

  if (util.isArray(opts.extensionsAllowed)) {
    opts.extensionsAllowed = opts.extensionsAllowed;
  } else {
    opts.extensionsAllowed = [];
  }
  opts.extensionsAllowed = opts.extensionsAllowed || [];
  opts.verbose = process.argv.indexOf('--verbose') !== -1;


  return through.obj(function (file, enc, callbackStream) {

    var currentStream = this;
    var cache = [];

    if (file.isNull()) {
      currentStream.push(file);
      return callbackStream();
    }

    if (file.isBuffer()) {
      var src = file.contents.toString();
      var result = [];
      var urlArr = [];
      
      async.whilst(
        function () {
          result = reg.exec(src);
          return result !== null;
        },
        function (callback) {
          if (cache[result[1]]) {
            src = src.replace(result[1], cache[result[1]]);
            callback();
            return;
          }
          var pureUrl = result[1].split('?')[0].split('#')[0];

          if (opts.extensionsAllowed.length !== 0 && opts.extensionsAllowed.indexOf(path.extname(pureUrl)) === -1) {
            callback();
            return;
          }else{
            arr.push(pureUrl);
            callback();
            return;
          }
        },function(){
          fs.writeFile('output.txt', arr.join('\r\n'));
          return callbackStream();
        });
    }
  });
}

module.exports = gulpCssUrlExtract;